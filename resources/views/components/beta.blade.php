@if ($background === 'dark')
<span class="ml-1 badge beta-badge text-white">
    Beta
</span>
@else
<span class="ml-1 badge beta-badge text-dark">
    Beta
</span>
@endif
